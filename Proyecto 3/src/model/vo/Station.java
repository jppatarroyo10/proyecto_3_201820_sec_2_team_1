package model.vo;

import java.time.LocalDateTime;

public class Station implements Comparable<Station> {
	

	private int stationId;
	private String stationName;
	private String city;
	private double latitude;
	private double longitude;
	private int dpcapacity;
	private LocalDateTime startDate;
	private int puntos;
	private double distancia;

	public Station(int stationId, String stationName, String city,
			double latitude, double longitude, int dpcapacity,
			LocalDateTime startDate, int puntos) {
		this.stationId = stationId;
		this.stationName = stationName;
		this.city = city;
		this.latitude = latitude;
		this.longitude = longitude;
		this.dpcapacity = dpcapacity;
		this.startDate = startDate;
		this.puntos = puntos;
	}

	public int compareTo(Station o) {
		if(puntos < o.puntos) return -1;
		else if(puntos > o.puntos) return 1;
		else return 0;
	}

	public int getStationId() {
		return stationId;
	}

	public void setStationId(int stationId) {
		this.stationId = stationId;
	}

	public String getStationName() {
		return stationName;
	}

	public void setStationName(String stationName) {
		this.stationName = stationName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public int getDpcapacity() {
		return dpcapacity;
	}

	public void setDpcapacity(int dpcapacity) {
		this.dpcapacity = dpcapacity;
	}

	public LocalDateTime getStartDate() {
		return startDate;
	}

	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}

	public int getPuntos(){
		return puntos;
	}
	
	public void setPuntos(int puntos){
		this.puntos += puntos;
	}
	
	public double getDistancia(){
		return distancia;
	}
	
	public void setDistancia(double distancia){
		this.distancia = distancia;
	}

}
