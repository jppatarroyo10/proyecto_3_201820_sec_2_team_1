package model.logic;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import API.IDivvyTripsManager;
import model.data_structures.IGraph;
import model.data_structures.IList;
import model.data_structures.LinkedListQueue;
import model.data_structures.RedBlackBST;
import model.vo.ComponenteFuertementeConectada;
import model.vo.Intersection;
import model.vo.Path;
import model.vo.Station;

public class DivvyTripsManager implements IDivvyTripsManager {
	
	private LinkedListQueue<Intersection> queueVertices;
	private RedBlackBST<Integer, Intersection> arbolVertices;
	
	private void cargarVertices(){
		String file = "./Data/Chicago Street Lines/Nodes_of_Chicago_Street_Lines.txt";
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		String lines = "";
		try {
			while((lines=reader.readLine())!=null){
				String[] arr = lines.split(",");
				int id = Integer.parseInt(arr[0]);
				double latitud = Double.parseDouble(arr[1]);
				double longitud = Double.parseDouble(arr[2]);
				Intersection inter = new Intersection(id, latitud, longitud);
				queueVertices.enqueue(inter);
				arbolVertices.put(inter.getId(), inter);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void cargarSistema() {
		// TODO Auto-generated method stub
	}

	public Path A1_menorDistancia(double latInicial, double lonInicial, double latFinal, double lonFinal) {
		//TODO Visualizacion Mapa
		return null;
	}

	public Path A2_menorNumVertices(double latInicial, double lonInicial, double latFinal, double lonFinal) {
		//TODO Visualizacion Mapa
		return null;
	}

	public IList<Station> B1_estacionesCongestionadas(int n) {
		//TODO Visualizacion Mapa
		return null;
	}

	public IList<Path> B2_rutasMinimas(IList<Station> stations) {
		//TODO Visualizacion Mapa
		return null;
	}

	public IGraph C1_grafoEstaciones() {
		//TODO Visualizacion Mapa
		return null;
	}
	
	public void C1_persistirGrafoEstaciones(IGraph grafoEstaciones){
		//TODO Visualizacion Mapa
	}

	public IList<ComponenteFuertementeConectada> C2_componentesFuertementeConectados() {
		//TODO Visualizacion Mapa
		return null;
	}

	public void C3_pintarGrafoEstaciones(IGraph grafoEstaciones) {
		//TODO Visualizacion Mapa!
		
	}
}
