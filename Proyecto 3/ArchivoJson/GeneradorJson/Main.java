package GeneradorJson;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import model.data_structures.LinkedListQueue;
import model.vo.Intersection;
import model.vo.Station;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.opencsv.CSVReader;


public class Main {
	
	public static final String STATIONS_Q1_Q2 = "./Data/Divvy_Trips_2017_Q1Q2/Divvy_Stations_2017_Q1Q2.csv";
	public static final String STATIONS_Q3_Q4 = "./Data/Divvy_Trips_2017_Q3Q4/Divvy_Stations_2017_Q3Q4.csv";
	
	private static LinkedListQueue<Intersection> queueVertices;
	
	private static void cargarVertices(){
		queueVertices = new LinkedListQueue<>();
		String file = "./Data/Chicago Street Lines/Nodes_of_Chicago_Street_Lines.txt";
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		String lines = "";
		try {
			while((lines=reader.readLine())!=null){
				String[] arr = lines.split(",");
				int id = Integer.parseInt(arr[0]);
				double latitud = Double.parseDouble(arr[1]);
				double longitud = Double.parseDouble(arr[2]);
				Intersection inter = new Intersection(id, latitud, longitud);
				queueVertices.enqueue(inter);
				//arbolVertices.put(inter.getId(), inter);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static void cargarAristas(LinkedListQueue<Integer[]> cola){
		String file = "./Data/Chicago Street Lines/Adjacency_list_of_Chicago_Street_Lines.txt";
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		String lines = "";
		try {
			lines = reader.readLine();
			lines = reader.readLine();
			lines = reader.readLine();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		try {
			while((lines=reader.readLine())!=null){
				String[] arr = lines.split(" ");
				Integer[] aux = new Integer[arr.length];
				for(int i=0; i<arr.length; i++){
					aux[i] = Integer.parseInt(arr[i]);
				}
				cola.enqueue(aux);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private static Double toRad(Double value) {
		return value * Math.PI / 180;
	}
	
	private static double distancia(double lat1, double lon1, double lat2, double lon2){
		final int R = 6371*1000;

		double latDistance = toRad(lat2-lat1);
		double lonDistance = toRad(lon2-lon1);
		double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + 
				Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * 
				Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
		double distance = R * c;

		return distance;
	}
	
	private static int interseccionMasCercana(Station station){
		int cercano = 0;
		double distanciaMin = Double.MAX_VALUE;
		double lat1 = station.getLatitude();
		double lon1 = station.getLongitude();
		for(Intersection inter : queueVertices){
			double lat2 = inter.getLatitud();
			double lon2 = inter.getLongitud();
			double dis = distancia(lat1, lon1, lat2, lon2);
			if(dis < distanciaMin){
				distanciaMin = dis;
				cercano = inter.getId();
			}
		}
		return cercano;
	}
	
	private static void loadStations(String stationsFile, LinkedListQueue<Station> cola) {

		CSVReader reader;
		try {
			reader = new CSVReader(new FileReader(stationsFile));
			String[] nextLine;
			try {
				nextLine = reader.readNext();
				nextLine = reader.readNext();
				while (nextLine != null) {
					int id;
					String name;
					String city;
					double latitude;
					double longitude;
					int dpcapacity;
					LocalDateTime online_date;

					if (nextLine[0] != "") {
						id = Integer.parseInt(nextLine[0]);
					} else {
						id = 0;
					}

					name = nextLine[1];
					city = nextLine[2];

					if (nextLine[3] != "") {
						latitude = Double.parseDouble(nextLine[3]);
					} else {
						latitude = 0;
					}

					if (nextLine[4] != "") {
						longitude = Double.parseDouble(nextLine[4]);
					} else {
						longitude = 0;
					}

					if (nextLine[5] != "") {
						dpcapacity = Integer.parseInt(nextLine[5]);
					} else {
						dpcapacity = 0;
					}
					if(stationsFile == STATIONS_Q3_Q4){
						DateTimeFormatter format = DateTimeFormatter
								.ofPattern("M/d/yyyy H:mm");
						online_date = LocalDateTime.parse(nextLine[6], format);

						Station station = new Station(id, name, city, latitude,
								longitude, dpcapacity, online_date, 0);
						cola.enqueue(station);
					}
					else if(stationsFile == STATIONS_Q1_Q2){
						DateTimeFormatter format = DateTimeFormatter
								.ofPattern("M/d/yyyy HH:mm:ss");
						online_date = LocalDateTime.parse(nextLine[6], format);

						Station station = new Station(id, name, city, latitude,
								longitude, dpcapacity, online_date, 0);
						cola.enqueue(station);
					}

					nextLine = reader.readNext();
				}

			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			System.out.println("no se encontr� el archivo");
			e1.printStackTrace();
		}
	}

	private static int maxId( LinkedListQueue<Station> cola){
		int max = 0;
		int id = 0;
		for(Station station : cola){
			id = station.getStationId();
			if(id > max){
				max = id;
			}
		}
		return max;
	}
	
	public static void main(String[] args){
		
		
		
		cargarVertices();
		LinkedListQueue<Integer[]> cola = new LinkedListQueue<>();		
		cargarAristas(cola);
		
		LinkedListQueue<Station> queueStations = new LinkedListQueue<>();
		loadStations(STATIONS_Q1_Q2, queueStations);
		loadStations(STATIONS_Q3_Q4, queueStations);
		
		for(Station station : queueStations){
			int adyacente = 0;
			Integer[] arr = new Integer[2];
			adyacente = interseccionMasCercana(station);
			arr[0] = station.getStationId() + 321375;
			arr[1] = adyacente;
			cola.enqueue(arr);
		}
		
		int numVer = maxId(queueStations) + 321375;
		try {
			FileWriter file = new FileWriter("./Data/Grafo-Chicago.json");
			
			file.write("[\n");
			file.write("\tn�mero v�rtices:" + numVer + ",\n");
			file.write("\tv�rtices:[\n");
			
			int j=0;
			for(Integer[] arr : cola){
				j++;
				if(arr.length == 1){
					JsonParser parser = new JsonParser();
					JsonObject obj = new JsonObject();
					JsonElement js = parser.parse(Integer.toString(arr[0]));
					obj.add("Id", js);
					JsonArray lista = new JsonArray();
					lista.add(null);
					obj.add("Adyacentes", lista);
					if(j==cola.getSize()-1){
					
					}
					else{
						file.write("\t\t");
						file.write(obj.toString());
					}
				}
				else{
					JsonParser parser = new JsonParser();
					JsonObject obj = new JsonObject();
					JsonElement js = parser.parse(Integer.toString(arr[0]));
					obj.add("Id", js);
					JsonArray lista = new JsonArray();
					for(int i=1; i<arr.length; i++){
						lista.add(parser.parse(Integer.toString(arr[i])));
					}
					obj.add("Adyacentes", lista);
					file.write("\t\t");
					file.write(obj.toString());
				}
				if(j != cola.getSize()){
					file.write(",\n");
				}
			}
			file.write("\n\t]\n");
			file.write("]\n");
			file.close();
		} catch (IOException e) {
			e.printStackTrace();
		}	
		System.out.println("Ya");
	}


}
